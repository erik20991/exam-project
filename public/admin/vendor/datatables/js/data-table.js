jQuery(document).ready(function($) {
    'use strict';

    // table for dropdowns
    if ($("table.dropdowns").length) {

        $(document).ready(function() {
            var table = $('table.table').DataTable({
                lengthChange: false,
                buttons: ['copy', 'excel', 'pdf', 'print', 'colvis'],
            });

            table.buttons().container()
                .appendTo('#table_wrapper .col-md-6:eq(0)');
        });
    }

    if ($("#headoffice.dropdowns").length) {

        $(document).ready(function() {
            var table = $('#headoffice.table').DataTable();

            table.buttons().container()
                .appendTo('#headoffice_wrapper .col-md-6:eq(0)');
        });
    }

    /* Calender jQuery **/

    // employee table
    if ($("table.employee").length) {

        $(document).ready(function() {
            // Setup - add a text input to each footer cell
            $('#employee tfoot th').each(function() {
                var title = $(this).text();
                $(this).html('<input type="text" class="form-control" placeholder="Search ' + title + '" />');
            });

            var table = $('table.employee').DataTable({
                'lengthChange': false,
                'pageLength': 25,
                'buttons': ['copy', 'excel', 'colvis'],
                'columnDefs': [{
                        'targets': [6, 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61],
                        'visible': false,
                        // searchable: false,
                    },
                    { 'orderable': false, 'targets': [0], }
                ],
                "order": [1, 'asc'],
                "scrollX": true,
            });

            table.columns.adjust().draw();

            table.buttons().container()
                .appendTo('#employee_wrapper .col-md-6:eq(0)');

            table.columns().every(function() {
                var that = this;

                $('input', this.footer()).on('keyup change clear', function() {
                    if (that.search() !== this.value) {
                        that
                            .search(this.value)
                            .draw();
                    }
                });
            });
        });
    }

    // if ($("table.second").length) {

    //     $(document).ready(function() {
    //         var table = $('table.second').DataTable({
    //             lengthChange: false,
    //             buttons: ['copy', 'excel', 'pdf', 'print', 'colvis'],
    //             columnDefs: [
    //                 { targets: 1, visible: false },
    //                 { targets: '_all', visible: false }
    //             ],
    //         });

    //         table.buttons().container()
    //             .appendTo('#example_wrapper .col-md-6:eq(0)');
    //     });
    // }

    // if ($("#example2").length) {

    //     $(document).ready(function() {
    //         $(document).ready(function() {
    //             var groupColumn = 2;
    //             var table = $('#example2').DataTable({
    //                 "columnDefs": [
    //                     { "visible": false, "targets": groupColumn }
    //                 ],
    //                 "order": [
    //                     [groupColumn, 'asc']
    //                 ],
    //                 "displayLength": 25,
    //                 "drawCallback": function(settings) {
    //                     var api = this.api();
    //                     var rows = api.rows({ page: 'current' }).nodes();
    //                     var last = null;

    //                     api.column(groupColumn, { page: 'current' }).data().each(function(group, i) {
    //                         if (last !== group) {
    //                             $(rows).eq(i).before(
    //                                 '<tr class="group"><td colspan="5">' + group + '</td></tr>'
    //                             );

    //                             last = group;
    //                         }
    //                     });
    //                 }
    //             });

    //             // Order by the grouping
    //             $('#example2 tbody').on('click', 'tr.group', function() {
    //                 var currentOrder = table.order()[0];
    //                 if (currentOrder[0] === groupColumn && currentOrder[1] === 'asc') {
    //                     table.order([groupColumn, 'desc']).draw();
    //                 } else {
    //                     table.order([groupColumn, 'asc']).draw();
    //                 }
    //             });
    //         });
    //     });
    // }
});