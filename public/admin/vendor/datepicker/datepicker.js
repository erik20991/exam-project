jQuery(document).ready(function($) {
    'use strict';

    if ($("#datetimepicker1").length) {
        $('#datetimepicker1').datetimepicker();

    }

    /* Calender jQuery **/
    if ($("#fatherBirthDate").length) {
        $('#fatherBirthDate').datetimepicker({
            viewMode: 'years',
            format: 'YYYY-MM-DD'
        });
    }

    if ($("#motherBirthDate").length) {
        $('#motherBirthDate').datetimepicker({
            viewMode: 'years',
            format: 'YYYY-MM-DD'
        });
    }

    if ($("#spouseBirthDate").length) {
        $('#spouseBirthDate').datetimepicker({
            viewMode: 'years',
            format: 'YYYY-MM-DD'
        });
    }

    if ($("#childBirthDate").length) {
        $('#childBirthDate').datetimepicker({
            viewMode: 'years',
            format: 'YYYY-MM-DD'
        });
    }

    if ($("#academicYearTo").length) {
        $('#academicYearTo').datetimepicker({
            viewMode: 'years',
            format: 'MMMM YYYY',
        });
    }

    if ($("#academicYearFrom").length) {
        $('#academicYearFrom').datetimepicker({
            viewMode: 'years',
            format: 'MMMM YYYY',
        });
    }

    if ($("#datetimepicker3").length) {
        $('#datetimepicker3').datetimepicker({
            format: 'LT'
        });
    }

    if ($("#birthdate").length) {
        $('#birthdate').datetimepicker({
            viewMode: 'years',
            format: 'YYYY-MM-DD'
        });
    }

    if ($("#datetimepicker5").length) {
        $('#datetimepicker5').datetimepicker();

    }

    if ($("#datetimepicker6").length) {
        $('#datetimepicker6').datetimepicker({
            defaultDate: "11/1/2013",
            disabledDates: [
                moment("12/25/2013"),
                new Date(2013, 11 - 1, 21),
                "11/22/2013 00:53"
            ],
            icons: {
                time: "far fa-clock",
                date: "fa fa-calendar-alt",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            }
        });
    }

    if ($("#datetimepicker7").length) {
        $(function() {
            $('#datetimepicker7').datetimepicker({
                icons: {
                    time: "far fa-clock",
                    date: "fa fa-calendar-alt",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                }
            });
            $('#datetimepicker8').datetimepicker({
                useCurrent: false,
                icons: {
                    time: "far fa-clock",
                    date: "fa fa-calendar-alt",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                }
            });
            $("#datetimepicker7").on("change.datetimepicker", function(e) {
                $('#datetimepicker8').datetimepicker('minDate', e.date);
            });
            $("#datetimepicker8").on("change.datetimepicker", function(e) {
                $('#datetimepicker7').datetimepicker('maxDate', e.date);
            });
        });
    }



    if ($("#datetimepicker10").length) {
        $('#datetimepicker10').datetimepicker({
            viewMode: 'years',
            icons: {
                time: "far fa-clock",
                date: "fa fa-calendar-alt",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            }
        });
    }

    if ($("#datetimepicker11").length) {
        $('#datetimepicker11').datetimepicker({
            viewMode: 'years',
            format: 'MM/YYYY'
        });
    }

    if ($("#datetimepicker13").length) {
        $('#datetimepicker13').datetimepicker({
            inline: true,
            sideBySide: true
        });

    }

    if ($("#startDate").length) {
        $('#startDate').datetimepicker({
            viewMode: 'months',
            format: 'YYYY-MM-DD'
        });
    }

    if ($("#regularizationDate").length) {
        $('#regularizationDate').datetimepicker({
            viewMode: 'months',
            format: 'YYYY-MM-DD'
        });
    }

    if ($("#endDate").length) {
        $('#endDate').datetimepicker({
            viewMode: 'months',
            format: 'YYYY-MM-DD'
        });
    }

    if ($("#dateCleared").length) {
        $('#dateCleared').datetimepicker({
            viewMode: 'months',
            format: 'YYYY-MM-DD'
        });
    }

    if ($("#tenure").length) {
        $('#tenure').datetimepicker({
            viewMode: 'months',
            format: 'YYYY-MM-DD'
        });
    }

    if ($("#jobOfferDate").length) {
        $('#jobOfferDate').datetimepicker({
            viewMode: 'months',
            format: 'YYYY-MM-DD'
        });
    }
});