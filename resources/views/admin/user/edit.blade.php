@extends('admin.layout.app') 

@section('content')

    <div class="row">
        <div class="forms">
            <form action="{{ action('UserController@update', $user->id) }}" method='post' enctype='multipart/form-data'>
                @csrf
                @method('PUT')
                <h2 class='title1'>User</h2>
                <div class="form-grids row widget-shadow" data-example-id="basic-forms">
                    <div class="form-title">
                        <h4>Create New User</h4>
                    </div>
                    <div class="form-body">
                        <div class="row col-md-12">
                            <div class="form-group">
                                <label class="">Full Name <span class='text-danger'>*</span></label>
                                <div class="row">
                                    <div class="col-md-2 px-0">
                                        <select name="prefix" class='form-control'>
                                            <option value="">Select Prefix</option>
                                            <option value="Mr." {{ $user->prefixName ==  "Mr." ? 'selected' : '' }}>Mr.</option>
                                            <option value="Ms." {{ $user->prefixName ==  "Ms." ? 'selected' : '' }}>Ms.</option>
                                            <option value="Mrs." {{ $user->prefixName ==  "Mrs." ? 'selected' : '' }}>Mrs.</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 px-0">
                                        <input type="text" name='lastName' class="form-control" placeholder="Last name *" value='{{ $user->lastName }}' style="text-transform: capitalize;">
                                    </div>
                                    <div class="col-md-3 px-0">
                                        <input type="text" name='firstName' class="form-control" placeholder="First name *" value='{{ $user->firstName }}' style="text-transform: capitalize;">
                                    </div>
                                    <div class="col-md-3 px-0">
                                        <input type="text" name='middleName' class="form-control" placeholder="Middle name" value='{{ $user->middleName }}' style="text-transform: capitalize;">
                                    </div>
                                    <div class="col-md-1 px-0">
                                        <input type="text" name='suffix' class="form-control" placeholder="Suffix" value='{{ $user->suffix }}' style="text-transform: capitalize;">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group"> 
                                <label for="exampleInputEmail1">Username <span class='text-danger'>*</span></label> 
                                <input type="text" name='username' class="form-control" placeholder="Username..." value="{{ $user->username }}"> 
                            </div>
                            <div class="form-group"> 
                                <label for="exampleInputEmail1">Password <span class='text-danger'>*</span></label> 
                                <input type="password" name='password' class="form-control" placeholder="Password..." value="" disabled> 
                            </div>
                            <div class="form-group"> 
                                <label for="">Email <span class='text-danger'>*</span></label> 
                                <input type="email" name='email' class="form-control" placeholder="example@gmail.com" value="{{ $user->email }}"> 
                            </div>
                            <div class="form-group">
                                <label class="col-form-label" for="imgInp">Image</label>
                                <input type="file" name='photo' class="form-control" id='imgInp'>
                                <input type="hidden" name="photo">
                                <p>Please upload a 1x1 photo, Max of 2MB</p>
                            </div>
                        </div>
                        {{-- <div class="row col-md-6">
                            <div class="form-group"> 
                                <label for="">Email <span class='text-danger'>*</span></label> 
                                <input type="email" name='email' class="form-control" placeholder="example@gmail.com"> 
                            </div>
                            <div class="form-group">
                                <label class="col-form-label" for="imgInp">Image</label>
                                <input type="file" name='image' class="form-control" id='imgInp'>
                                <p>Please upload a 1x1 photo, Max of 2MB</p>
                            </div>
                        </div> --}}
                    </div>
                </div>
                <div class="row">
                    <div class="ml-auto mr-3">
                        <button type='submit' class="btn btn-success">Submit</button>
                        <a href='{{ url('/user') }}' class="btn btn-danger">Back</a>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection

@section('script')
    <script>
        $(document).ready(function(){
            //
        });
    </script>
@endsection