@extends('admin.layout.app')

@section('content')
    <div class="panel panel-default mx-3">
        <div class="panel-heading" role="tab" id="headingOne">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#profile" aria-expanded="true" aria-controls="profile">
                    User's Profile
                </a>
            </h4>
        </div>
        <div id="profile" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
                <div class="col-xl-2 col-lg-4 col-md-4 col-sm-4">
                    <div class="text-center">
                        {!!$user->getAvatar()!!}
                    </div>
                </div>
                <div class="col-xl-10 col-lg-8 col-md-8 col-sm-8">
                    <div class="">
                        <div class="">
                            <div class="user-avatar-name">
                                <h1 class="mb-1">{{$user->prefixName}} {{$user->getFullName()}}</h1>
                            </div>
                        </div>
                        <!--  <div class="float-right"><a href="#" class="user-avatar-email text-secondary">www.henrybarbara.com</a></div> -->
                        <div class="">
                            <p class="border-bottom">
                                {{-- <a href="#" class="badge badge-info">{{$employee->office_information->branch->branchName}}</a>
                                <a href="#" class="badge badge-info">{{$employee->office_information->department->departmentName}}</a>
                                <a href="#" class="badge badge-info">{{$employee->office_information->cost_center->CostCenter}}</a>
                                <a href="#" class="badge badge-info">{{$employee->employment->jobGrade_tbl->jobGradeTitle}}</a>
                                <a href="#" class="badge badge-info">{{$employee->employment->jobCategory_tbl->jobCategoryTitle}}</a>
                                <a href="#" class="badge badge-info">{{$employee->employment->position_tbl->positionTitle}}</a> --}}
                                <a href="#" class="badge badge-info">{{ucfirst($user->type)}}</a>
                                <a href="#" class="badge badge-info">{{$user->username}}</a>
                            </p>
                            <div class="mt-3">
                                <hr>
                                {{-- <span class="d-xl-inline-block d-block mb-2"><i class="fa fa-map-marker mr-2 text-primary "></i>{{$employee->personal->permanentAddress != null ? $employee->personal->permanentAddress : 'None'}}</span>
                                <span class="mb-2 ml-xl-4 d-xl-inline-block d-block">Start Date: {{date('F d, Y', strtotime($employee->employment->startDate))}}  </span>
                                <span class=" mb-2 d-xl-inline-block d-block ml-xl-4">{{$employee->personal->gender}} </span>
                                <span class=" mb-2 d-xl-inline-block d-block ml-xl-4">{{$employee->personal->age}} years old</span> --}}
                                <span class="d-xl-inline-block d-block mb-2"><i class="fa fa-envelope mr-2 text-primary "></i>{{$user->email}} </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="ml-auto mr-3">
            <a href='{{ url('/user') }}' class="btn btn-danger">Back</a>
        </div>
    </div>
@endsection
