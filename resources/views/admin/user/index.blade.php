
@extends('admin.layout.app')

@section('style')
    <style>
        .profile{
            width: 50px;
        }
    </style>
@endsection

@section('content')

    <div class="row">
        <div class="forms">
            <h2 class="title1">Users List</h2>
            <div class="col-md-12 panel-grids">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Users</h3>
                    </div>
                    <div class="panel-body">
                        <a href="{{ url('user/create') }}" class='btn btn-success btn-block mb-3'><i class='fa fa-plus'></i> Create User  </a>
                        <div class="table-responsive">
                            <table id="custom-table" class="table table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Avatar</th>
                                        <th>Prefix</th>
                                        <th>First Name</th>
                                        <th>Middle Name</th>
                                        <th>Last Name</th>
                                        <th>Suffix</th>
                                        <th width='170'>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($users as $user)
                                        <tr>
                                            <td class="text-center">{!!$user->getAvatar()!!}</td>
                                            <td>{{ $user->prefixName != null ? $user->prefixName : 'None' }}</td>
                                            <td>{{ $user->firstName }}</td>
                                            <td>{{ $user->middleName != '' ? $user->middleName : 'None' }}</td>
                                            <td>{{ $user->lastName }}</td>
                                            <td>{{ $user->suffix != '' ? $user->suffix : 'None' }}</td>
                                            <td>
                                                <div class="btn-group" role="group">
                                                    <form action="{{action('UserController@show', $user->id)}}" method='get' style="display: inline"><button type="submit" class="btn btn-primary"><i class="fa fa-eye" aria-hidden="true"></i></button></form>
                                                    <form action="{{action('UserController@edit', $user->id)}}" method='get' style="display: inline"><button type="submit" class="btn btn-success"><i class='fa fa-pencil-square-o'></i></button></form>
                                                    <form action="{{action('UserController@destroy', $user->id)}}" method='post' style="display: inline">@csrf @method('DELETE')<button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this item?');"><i class="fa fa-trash-o" aria-hidden="true"></i></button></form>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script>
        // datatable
        $(document).ready(function(){
            $('#custom-table').DataTable({
                'columnDefs':[
                    {'orderable': false, 'targets': [1]},
                ],
            });
        })
    </script>
@endsection