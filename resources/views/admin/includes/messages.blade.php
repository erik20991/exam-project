@if(count($errors) > 0)
    <div class="alert alert-danger">
        <button type='button' class='close'data-dismiss='alert'>x</button>
        <p>Please input all the required information!</p>
        <ul>
            <span style="display:none">{!! $count=0 !!}</span>
        @foreach($errors->all() as $error)
            @if($count==0)
                <li>{{$error}}</li>
                <span style="display:none">{!! $count++ !!}</span>
            @endif
        @endforeach
        </ul>
    </div>
@endif

@if(session('success'))
    <div class="alert alert-success alert-block">
        <button class="close" data-dismiss='alert'>x</button>
        <strong>{{ session('success') }}</strong>
    </div>
@endif

@if(session('error'))
    <div class="alert alert-danger alert-block">
        <button class="close" data-dismiss='alert'>x</button>
        <strong>{{ session('error') }}</strong>
    </div>
@endif