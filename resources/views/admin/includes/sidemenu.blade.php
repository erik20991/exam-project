<div class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
    <!--left-fixed -navigation-->
    <aside class="sidebar-left">
        <nav class="navbar navbar-inverse">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".collapse" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            <a class="navbar-brand" href="{{ url('/user') }}"><h3><img src="" alt="" width="50"> Users</h3></a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="sidebar-menu">
                    <li class="header">MAIN NAVIGATION </li>
                    <li class="treeview">
                        <a href="{{url('/')}}">
                            <i class="fa fa-tachometer"></i> <span>Dashboard</span>
                        </a>
                    </li>
                    
                    {{-- <li class="treeview">
                        <a href="{{url('/user')}}">
                            <i class="fa fa-user"></i>
                            <span>Users</span>
                        </a>
                    </li> --}}

                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-lock"></i>
                            <span>User</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="{{url('/user')}}"><i class="fa fa-angle-right"></i> User List</a></li>
                            <li><a href="{{url('trashed/user')}}"><i class="fa fa-angle-right"></i> Deleted User List</a></li>
                        </ul>
                    </li>
                </ul>
            </div>

            <!-- /.navbar-collapse -->
        </nav>
    </aside>
</div>
<!--left-fixed -navigation-->
