<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>

<head>
    <title>User</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Glance Design Dashboard Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
    SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    <script type="application/x-javascript">
        addEventListener("load", function() {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>

    <!-- Bootstrap Core CSS -->
    <link href="{{asset('admin/css/bootstrap.css')}}" rel='stylesheet' type='text/css' />

    <!-- Custom CSS -->
    <link href="{{asset('admin/css/style.css')}}" rel='stylesheet' type='text/css' />

    <!-- font-awesome icons CSS -->
    <link href="{{asset('admin/css/font-awesome.css')}}" rel="stylesheet">
    {{-- <link rel="stylesheet" href="{{asset('admin/vendor/fonts/fontawesome/css/fontawesome-all.css')}}"> --}}
    <!-- //font-awesome icons CSS-->

    <!-- side nav css file -->
    <link href='{{asset('admin/css/SidebarNav.min.css')}}' media='all' rel='stylesheet' type='text/css' />
    <!-- //side nav css file -->

    <!-- js-->
    <script src="{{asset('admin/js/jquery-1.11.1.min.js')}}"></script>
    <script src="{{asset('admin/js/modernizr.custom.js')}}"></script>

    <!--webfonts-->
    <link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext"
        rel="stylesheet">
    <!--//webfonts-->

    <!-- Metis Menu -->
    <script src="{{asset('admin/js/metisMenu.min.js')}}"></script>
    <script src="{{asset('admin/js/custom.js')}}"></script>
    <link href="{{asset('admin/css/custom.css')}}" rel="stylesheet">

    <link href="{{ asset('admin/assets/select2/select2.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('admin/assets/select2/select2-bootstrap.css') }}" rel="stylesheet" />
    <link href="{{ asset('admin/assets/select2/select2-bootstrap.min.css') }}" rel="stylesheet" />

    <link rel="stylesheet" href="{{asset('admin/assets/datepicker1/css/bootstrap-datepicker3.css')}}" type="text/css" />

    <link rel="stylesheet" href="{{asset('admin/assets/datetimepicker/css/bootstrap-datetimepicker.css')}}" type="text/css" />
    
    <link rel="stylesheet" href="{{asset('admin/assets/calendar/mini-calendar/css/mini-event-calendar.min.css')}}">
    
    @yield('style')
</head>

<body class="cbp-spmenu-push">
    <div class="main-content">
        <!-- sidebar -->
        @include('admin.includes.sidemenu')
        <!-- header -->
        @include('admin.includes.header')
        <!-- main content start-->
        <div id="page-wrapper">
            <div class="main-page">
                @include('admin.includes.messages')
                <!-- content -->
                @yield('content')
            </div>
        </div>
        <!--footer-->
        <div class="footer">
            <p>&copy; 2018 Glance Design Dashboard. All Rights Reserved | Design by <a href="https://w3layouts.com/"
                    target="_blank">w3layouts</a></p>
        </div>
        <!--//footer-->
    </div>

    <!-- Classie -->
    <!-- for toggle left push menu script -->
    <script src="{{asset('admin/js/classie.js')}}"></script>
    <script>
        var menuLeft = document.getElementById('cbp-spmenu-s1'),
            showLeftPush = document.getElementById('showLeftPush'),
            body = document.body;

        showLeftPush.onclick = function () {
            classie.toggle(this, 'active');
            classie.toggle(body, 'cbp-spmenu-push-toright');
            classie.toggle(menuLeft, 'cbp-spmenu-open');
            disableOther('showLeftPush');
        };


        function disableOther(button) {
            if (button !== 'showLeftPush') {
                classie.toggle(showLeftPush, 'disabled');
            }
        }
    </script>

    <!-- side nav js -->
    <script src='{{asset('admin/js/SidebarNav.min.js')}}' type='text/javascript'></script>
    <script>
        $('.sidebar-menu').SidebarNav()
    </script>
    <!-- //side nav js -->

    <!-- Bootstrap Core JavaScript -->
    <script src="{{asset('admin/js/bootstrap.js')}}"></script>
    <!-- //Bootstrap Core JavaScript -->

    <script type="text/javascript" src="{{URL('/')}}/admin/assets/datatables/JSZip-2.5.0/jszip.js"></script>
    <script type="text/javascript" src="{{URL('/')}}/admin/assets/datatables/pdfmake-0.1.36/pdfmake.js"></script>
    <script type="text/javascript" src="{{URL('/')}}/admin/assets/datatables/pdfmake-0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" src="{{URL('/')}}/admin/assets/datatables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="{{URL('/')}}/admin/assets/datatables/DataTables-1.10.18/js/dataTables.bootstrap.js"></script>
    <script type="text/javascript" src="{{URL('/')}}/admin/assets/datatables/Buttons-1.5.2/js/dataTables.buttons.js"></script>
    <script type="text/javascript" src="{{URL('/')}}/admin/assets/datatables/Buttons-1.5.2/js/buttons.bootstrap.js"></script>
    <script type="text/javascript" src="{{URL('/')}}/admin/assets/datatables/Buttons-1.5.2/js/buttons.colVis.js"></script>
    <script type="text/javascript" src="{{URL('/')}}/admin/assets/datatables/Buttons-1.5.2/js/buttons.flash.js"></script>
    <script type="text/javascript" src="{{URL('/')}}/admin/assets/datatables/Buttons-1.5.2/js/buttons.html5.js"></script>
    <script type="text/javascript" src="{{URL('/')}}/admin/assets/datatables/Buttons-1.5.2/js/buttons.print.js"></script>

    <script src="{{ asset('admin/assets/select2/select2.min.js') }}"></script>

    @yield('script')
</body>

</html>