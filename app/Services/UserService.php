<?php

namespace App\Services;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Services\UserServiceInterface;
use Illuminate\Support\Facades\Hash;

class UserService implements UserServiceInterface
{
    /**
     * The model instance.
     *
     * @var App\User
     */
    protected $model;

    /**
     * The request instance.
     *
     * @var \Illuminate\Http\Request
     */
    protected $request;

    /**
     * Constructor to bind model to a repository.
     *
     * @param \App\User                $model
     * @param \Illuminate\Http\Request $request
     */
    public function __construct(User $model, Request $request)
    {
        $this->model = $model;
        $this->request = $request;
    }

    /**
     * Define the validation rules for the model.
     *
     * @param  int $id
     * @return array
     */
    public function rules($id = null)
    {
        return [
            /**
             * Rule syntax:
             *  'column' => 'validation1|validation2'
             *
             *  or
             *
             *  'column' => ['validation1', function1()]
             */
            'prefixName' => 'nullable',
            'firstName' => 'required',
            'middleName' => 'nullable',
            'lastName' => 'required',
            'suffix' => 'nullable',
            'username' => 'required',
            'email' => 'required|email',
            'password' => 'nullable',
            'photo' => 'nullable|image|max:2000',
        ];
    }

    /**
     * Retrieve all resources and paginate.
     *
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function list()
    {
        $users = User::all();

        return $users;
    }

    /**
     * Create model resource.
     *
     * @param  array $attributes
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function store(array $attributes)
    {
        // return $attributes;
        $user = new User;
        $user->prefixName = $attributes['prefix'];
        $user->firstName = ucfirst($attributes['firstName']);
        $user->middleName = ucfirst($attributes['middleName']);
        $user->lastName = ucfirst($attributes['lastName']);
        $user->username = $attributes['username'];
        $user->password = $this->hash($attributes['password']);
        $user->email = $attributes['email'];
        if($attributes['photo'] != null){
            $user->photo = $this->upload($attributes['photo']);
        }else{
            $user->photo = 'default.png';
        }
        $user->save();
    }

    /**
     * Retrieve model resource details.
     * Abort to 404 if not found.
     *
     * @param  integer $id
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function find(int $id):? User
    {
        $user = User::find($id);

        return $user;
    }

    /**
     * Update model resource.
     *
     * @param  integer $id
     * @param  array   $attributes
     * @return boolean
     */
    public function update(int $id, array $attributes): bool
    {
        // return $attributes;
        $user = User::find($id);
        $user->prefixName = $attributes['prefix'];
        $user->firstName = ucfirst($attributes['firstName']);
        $user->middleName = ucfirst($attributes['middleName']);
        $user->lastName = ucfirst($attributes['lastName']);
        $user->username = $attributes['username'];
        $user->email = $attributes['email'];
        if($attributes['photo'] != null){
            $user->photo = $this->upload($attributes['photo']);
        }else{
            $user->photo = 'default.png';
        }
        $user->save();

        return true;
    }

    /**
     * Soft delete model resource.
     *
     * @param  integer|array $id
     * @return void
     */
    public function destroy($id)
    {
        User::find($id)->delete();
    }

    /**
     * Include only soft deleted records in the results.
     *
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function listTrashed()
    {
        $users = User::where('deleted_at', '<>', NULL)->withTrashed()->get();

        return view('admin.user.trashed')
            ->with('users', $users);
    }

    /**
     * Restore model resource.
     *
     * @param  integer|array $id
     * @return void
     */
    public function restore($id)
    {
        $user = User::where('id', $id)->withTrashed()->first();
        $user->deleted_at = NULL;
        $user->save();

        return redirect('trashed/user')->with('success', 'Successfuly restored a user!');
    }

    /**
     * Permanently delete model resource.
     *
     * @param  integer|array $id
     * @return void
     */
    public function delete($id)
    {
        $user = User::where('id', $id)->withTrashed()->first();
        $user->forceDelete();

        return redirect('trashed/user')->with('error', 'Permanently deleted a user!');
    }

    /**
     * Generate random hash key.
     *
     * @param  string $key
     * @return string
     */
    public function hash(string $key): string
    {
        $hashedPassword = Hash::make($key);

        return $hashedPassword;
    }

    /**
     * Upload the given file.
     *
     * @param  \Illuminate\Http\UploadedFile $file
     * @return string|null
     */
    public function upload(UploadedFile $file)
    {
        if($file != null)
        {
            $filenameWithExt = $file->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $file->getClientOriginalExtension();
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            $path = $file->storeAs('public/image', $fileNameToStore);
            return $fileNameToStore;
        }
        else{
            return 'default.png';
        }
    }
}