<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use HasFactory, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'prefixName',
        'firstName',
        'middleName',
        'lastName',
        'suffixName',
        'username',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getFullName(){
        
        return "{$this->firstName} {$this->middleName} {$this->lastName} {$this->suffix}";
    }

    public function getAvatar(){
        $url = url('');
        return "<img src='{$url}/storage/image/{$this->photo}' alt='User Avatar' width='225' class='profile'>";
    }

    public function getMiddleInitial(){
        $middleName = $this->middleName;
        $temp = explode(' ', $middleName);
        $middleInitial = '';
        foreach($temp as $result){
            $middleInitial = $result[0];
        }

        return $middleInitial;
    }
}
